package sortersTesterClasses;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

import interfaces.Sorter;
import sorterClasses.BubbleSortSorter;
import sorterClasses.InsertionSortSorter;
import sorterClasses.SelectionSortSorter;

public class EnteroTester implements Comparator<Entero> {

	//private static Sorter<Integer> sorter; 
		private static Random rnd; 
		private static ArrayList<Sorter<Entero>> sortersList = new ArrayList<>(); 
		
		public static void main(String[] args) { 
			sortersList.add(new BubbleSortSorter<Entero>()); 
			sortersList.add(new SelectionSortSorter<Entero>()); 
			sortersList.add(new InsertionSortSorter<Entero>()); 
			
			test("Sorting Using Default Comparator<Entero>", null); 
		
		}
		
		private static void test(String msg, Comparator<Entero> cmp) { 
			rnd = new Random(101); 

			System.out.println("\n\n*******************************************************");
			System.out.println("*** " + msg + "  ***");
			System.out.println("*******************************************************");
			
			Entero[] original, arr; 
			// generate random arrays is size i and test...
			for (int i=1; i<=20; i += 5) { 
				original = randomValues(i);
				showArray("\n ---Original array of size " + i + " to sort:", original); 
				
				for (int s=0; s<sortersList.size(); s++) {
					Sorter<Entero> sorter = sortersList.get(s); 
				    arr = original.clone(); 
				    sorter.sort(arr, cmp);
				    showArray(sorter.getName() + ": ", arr); 
				}
			}
		}

		private static void showArray(String msg, Entero[] original) {
			System.out.print(msg); 
			for (int i=0; i<original.length; i++) 
				System.out.print("\t" + original[i]); 
			System.out.println();
		}

		public static Entero[] randomValues(int i) {
			Entero[] a = new Entero[i];
			Entero b;
			for (int j=0; j<i; j++) {
				
				b = new Entero(rnd.nextInt(100));
				a[j] = b; 
		        }
			return a;
		}

		@Override
		public int compare(Entero o1, Entero o2) {
			// TODO Auto-generated method stub
			return 0;
		}

}
